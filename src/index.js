import React, { useState } from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import StarRating from './StarRating';


function Test ()
{
  // for resue case allowing the user to pass state as props , this is pass from here , but value set there 
  const [movieRating,setMovieRating]=useState(0)
  return (
      <div> 
      <StarRating color='white' maxRating={ 15 } onSetRating={setMovieRating} />
      <p>  The movei rated {movieRating} stars </p>
      </div>
  )
}

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
	<React.StrictMode>
    {/* <StarRating maxRating={ 5 }  messages={['F','D','C','B','A']} />
    <StarRating  size={24} color='red' />
    <StarRating />
    <Test/> */}

    <App/>

	</React.StrictMode>
);
