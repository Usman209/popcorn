import React from 'react';
import { useEffect, useRef, useState } from 'react';

export default function useLocalStorage(intialState, key) {
	const [ value, setValue ] = useState(function() {
		const storeValue = localStorage.getItem(key);
		return storeValue ? JSON.parse(storeValue) : intialState;
	});

	useEffect(
		function() {
			localStorage.setItem(key, JSON.stringify(value));
		},
		[ value, key ]
	);

	return [ value, setValue ];
}
