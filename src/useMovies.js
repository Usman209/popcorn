import React from 'react'
import { useEffect, useRef, useState } from "react";

export function useMovies (query,callback)
{

  const [ movies, setMovies ] = useState( [] );
  const [ isLoading, setIsLoading ] = useState( false )
  const [ error, setError ] = useState( '' )

    const KEY='2ece038e'

  
   useEffect( function ()
   {
     
     callback?.();
    const controller = new AbortController()
    async function fetchMovies (  )
    {
      try
      {
        setIsLoading( true )
        setError('')
        const res = await fetch( `http://www.omdbapi.com/?apikey=${ KEY }&s=${ query }`, { signal: controller.signal } )
        
        console.log( 'res====', res );

        if ( !res.ok )
        {
          throw new Error( 'some thing went wrong fetching movies' )
        }
        const data = await res.json()

        if ( data.Response === 'False' )
        {
          throw new Error( 'movie not found' )

        }
        console.log( 'data is :', data );
        setMovies( data.Search )
        setError('')
      }
      catch ( err )
      {
        console.log( err.message );
        if(err.name!=='AbortError')
        setError(err.message)
      }
      finally
      {
        setIsLoading( false )
      }
    }
// prevent multiple calls
    if ( query.length < 3 )
    {
      setMovies( [] )
      setError( '' )
      return
    }

    // handleCloseMovie()
    fetchMovies();

    return function ()
    {
      controller.abort()
      
    }
    
   }, [ query ] )
  return {movies,isLoading,error}
}
